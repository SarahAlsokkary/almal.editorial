package Helpers;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import com.ikdynamics.elmalnewseditorial.R;
import java.io.ByteArrayOutputStream;


public class Constants {
    private static ProgressDialog pDialog;
    public static final  String url = "http://66.226.74.85:9899/Service.svc/";

    /**
     * changeStatusBarColor method to change the StatusBar color if version > Lollipop
     * @param window   the current window for the activity.
     * @param context the activity in which statusBar color to be changed
     */
    public static void changeStatusBarColor(Window window, Context context){

        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(context,R.color.colorPrimaryDark));
        }
    }

    /**
     * getBytesFromBitmap to convert bitmap to bytes[]
     * @param bitmap the bitmap to be converted
     * @return array of bytes of the bitmap
     */
    public static byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap,158,158,true);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public static void showProgressDialog(Context context) {
        // Showing progress dialog
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("من فضلك انتظر!");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    /**
     * decodeBase64 converts base64 to a bitmap
     * @param input base64 String
     * @return scaled bitmap with size 99.8 kilobytes
     */
    public static Bitmap decodeBase64(String input){

        byte[] decodedBytes = Base64.decode(input, 0);
        Bitmap bitmap= BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap,158,158,true);
        Log.e("pic size", scaledBitmap.getByteCount()+" bytes");
        return  scaledBitmap;
    }

    public static void hideProgressDialog() {
        // Dismiss the progress dialog
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}