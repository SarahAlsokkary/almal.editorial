package Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.ikdynamics.elmalnewseditorial.AddNewsActivity;
import com.ikdynamics.elmalnewseditorial.R;
import java.util.List;
import Helpers.Constants;
import Models.TopicsModel;

public class SavedNewsAdapter extends
        RecyclerView.Adapter<SavedNewsAdapter.ViewHolder> {

    private static Context context;
    private final List<TopicsModel> topics;
    private Gson gson ;

    public SavedNewsAdapter(List<TopicsModel> topics, Context context) {
        this.topics = topics;
        SavedNewsAdapter.context = context;

    }

    @Override
    public SavedNewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView;
        ViewHolder viewHolder;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.cardview_news_row, null);
        viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SavedNewsAdapter.ViewHolder holder,  final int position) {

            holder.newsTitle.setText(topics.get(position).getArticle_Title());
        if(topics.get(position).getArticle_ImgeBytes()==null){
            Log.e("tag", "no pic inserted");
        }else {
            holder.newsImg.setImageBitmap(Constants.decodeBase64(topics.get(position).getArticle_ImgeBytes()));
        }


            holder.newsCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AddNewsActivity.class);
                    gson= new Gson();
//                    int pos = position;
                    String o = gson.toJson(topics.get(position));
                    intent.putExtra("newsToUpdate",o);
                    intent.putExtra("newsToUpdatePosition",position);
                    context.startActivity(intent);


                }

            });
    }

    @Override
    public int getItemCount() {
        return topics.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView newsTitle;
        final ImageView newsImg;
        final CardView newsCard;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            newsTitle = (TextView) itemLayoutView.findViewById(R.id.news_title);
            newsImg =(ImageView) itemLayoutView.findViewById(R.id.news_image);
            newsCard = (CardView) itemLayoutView.findViewById(R.id.news_row_card);
        }
    }
}
