package Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.ikdynamics.elmalnewseditorial.R;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import Models.SectionsModel;


public abstract class RadioAdapter<T> extends RecyclerView.Adapter<RadioAdapter.ViewHolder> {
    private int mSelectedItem = -1;
    final List<T> mItems;
    private final Context mContext;
    private SharedPreferences.Editor editor;
    private ArrayList<SectionsModel> secList;
    private final Dialog sec;
    private SectionsModel section;
    private HashMap<Integer, Integer> meMap;
    private final TextView textView;
    private final int objectSectionID;
    private final String objectSectionName;

    RadioAdapter(Context context, List<T> items, Dialog sectionsDialog, TextView t, int sectionID, String sectionName) {
        mContext = context;
        mItems = items;
        sec = sectionsDialog;
        textView=t;
        objectSectionID = sectionID;
        objectSectionName = sectionName;
    }

    @Override
    public void onBindViewHolder(RadioAdapter.ViewHolder viewHolder, final int position) {



        if(objectSectionID!=-1 && objectSectionName!=null) {

            textView.setText(objectSectionName);
            int pos = meMap.get(objectSectionID);
            viewHolder.mRadio.setChecked(position == pos);
        }

    else{
            viewHolder.mRadio.setChecked(false);
        }

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final View view = inflater.inflate(R.layout.cardview_topic_list, viewGroup, false);




        return new ViewHolder(view);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public final RadioButton mRadio;
        public final TextView mText;


        public ViewHolder(final View inflate) {
            super(inflate);
            mText = (TextView) inflate.findViewById(R.id.topic_title);
            mRadio = (RadioButton) inflate.findViewById(R.id.radioButton);
            SharedPreferences sharedPref = mContext.getSharedPreferences("myPrefs", Context.MODE_APPEND);
            editor =sharedPref.edit();

            final String sectionsList = sharedPref.getString("sectionsList", null);
            if(sectionsList==null){
                Toast.makeText(mContext, "sectionsList = null", Toast.LENGTH_SHORT).show();
            }else {
                Gson gson = new Gson();
                secList = new ArrayList<>(Arrays.asList(gson.fromJson(sectionsList, SectionsModel[].class)));

                meMap=new HashMap<>();
                for(int j = 0 ; j < secList.size();j++){
                    section = secList.get(j);
                    meMap.put(section.getSection_ID(),j);
                }
            }


            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    Log.e("tag", mSelectedItem + "--"+getAdapterPosition());
                    section = secList.get(getAdapterPosition());
                    int sectionID = section.getSection_ID();
                    String sectionName = section.getSection_Name();
                    editor.putString("sectionName", sectionName);
                    editor.putInt("sec", sectionID);
                    editor.apply();
                    notifyItemRangeChanged(0, mItems.size());
                    if(sectionName == null || sectionName.isEmpty()){
                        textView.setText("اضف القسم");
                    }else {
                        textView.setText(sectionName);
                        sec.dismiss();
                    }
                }
            };

            itemView.setOnClickListener(clickListener);
            mRadio.setOnClickListener(clickListener);
        }
    }

}
