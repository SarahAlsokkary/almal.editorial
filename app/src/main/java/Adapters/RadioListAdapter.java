package Adapters;

import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import java.util.ArrayList;

import Models.SectionsModel;


public class RadioListAdapter extends RadioAdapter<SectionsModel> {


    public RadioListAdapter(Context context, ArrayList<SectionsModel> sectionList, Dialog sectionD, TextView h, int sectionId , String sectionName) {
        super(context, sectionList, sectionD, h,sectionId, sectionName);
    }


    @Override
    public void onBindViewHolder(RadioAdapter.ViewHolder viewHolder, final int position) {
        super.onBindViewHolder(viewHolder, position);
        viewHolder.mText.setText(mItems.get(position).getSection_Name());

    }

}