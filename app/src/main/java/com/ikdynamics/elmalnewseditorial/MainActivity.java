package com.ikdynamics.elmalnewseditorial;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;
import Helpers.Constants;
import Models.TopicsModel;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {


    private ImageView firstCardSentImg;
    private ImageView firstCardSavedImg;
    private ImageView secondCardSavedImg;
    private ImageView secondCardSentImg;
    private CardView firstSavedCard;
    private CardView secondSavedCard;
    private TextView firstCardSentTitle;
    private TextView firstCardSavedTitle;
    private TextView secondCardSavedTitle;
    private TextView secondCardSentTitle;
    private TextView nothingSavedToShow;
    private SharedPreferences sharedPref;
    private Gson gson;
    private static CardView c1,c2;
    private static TextView nothingSentToShow;
    private static  LinearLayout sentLayout, savedLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activityResumed();
        Log.e("resume","activityVisible = true");
    }



    @Override
    protected void onPause() {
        super.onPause();
        App.activityPaused();
        Log.e("onPause","activityVisible = false");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        gson = new Gson();
        sharedPref = getSharedPreferences("myPrefs", Context.MODE_APPEND);
        ArrayList<TopicsModel> list_n;
        ArrayList<TopicsModel> list_v;

        if(sharedPref.contains("savedList")) {
            String savedList = sharedPref.getString("savedList", null);
           list_v = new ArrayList<>(Arrays.asList(gson.fromJson(savedList, TopicsModel[].class)));
            if(list_v.size()==0){
                secondSavedCard.setVisibility(View.INVISIBLE);
                firstSavedCard.setVisibility(View.INVISIBLE);
                sentLayout.setEnabled(false);
                nothingSentToShow.setVisibility(View.VISIBLE);
            }
        }
        if(sharedPref.contains("sentList")) {
            String sentList = sharedPref.getString("sentList", null);
             list_n = new ArrayList<>(Arrays.asList(gson.fromJson(sentList, TopicsModel[].class)));
            if(list_n.size()==0){
                c2.setVisibility(View.INVISIBLE);
                c1.setVisibility(View.INVISIBLE);
                sentLayout.setEnabled(false);
                nothingSentToShow.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * fireUi to set sent cards visibility to invisible when the service finishes sending topics
     */
    public static void fireUi(){
if(App.isActivityVisible()){
    if(c1.getVisibility()!=View.INVISIBLE||c2.getVisibility()!=View.INVISIBLE) {
        c1.setVisibility(View.INVISIBLE);
        c2.setVisibility(View.INVISIBLE);
        sentLayout.setEnabled(false);
        nothingSentToShow.setVisibility(View.VISIBLE);
    }
}

 }

    /**
     * sharedHasSentList to check if the shared has sentList and populate the 2 static cards with the data found
     * and handles visibility
     */
    private void sharedHasSentList(){
        if (sharedPref.contains("sentList")) {
            String savedList = sharedPref.getString("sentList", null);
            ArrayList<TopicsModel> list = new ArrayList<>(Arrays.asList(gson.fromJson(savedList, TopicsModel[].class)));
            if (list.size() == 0) {
                Log.e("TAG", "sent list is empty");
                sentLayout.setEnabled(false);
                c2.setVisibility(View.INVISIBLE);
                c1.setVisibility(View.INVISIBLE);
            }
            else if (list.size() == 1) {
                sentLayout.setEnabled(true);
                nothingSentToShow.setVisibility(View.INVISIBLE);
                c1.setVisibility(View.VISIBLE);
                firstCardSentTitle.setText(list.get(0).getArticle_Title());
                if (list.get(0).getArticle_ImgeBytes() != null) {
                    firstCardSentImg.setImageBitmap(Constants.decodeBase64(list.get(0).getArticle_ImgeBytes()));
                }
            } else {
                sentLayout.setEnabled(true);
                nothingSentToShow.setVisibility(View.INVISIBLE);
                c2.setVisibility(View.VISIBLE);
                c1.setVisibility(View.VISIBLE);
                firstCardSentTitle.setText(list.get(0).getArticle_Title());
                if (list.get(0).getArticle_ImgeBytes() != null) {
                    firstCardSentImg.setImageBitmap(Constants.decodeBase64(list.get(0).getArticle_ImgeBytes()));
                }
                secondCardSentTitle.setText(list.get(1).getArticle_Title());
                if (list.get(1).getArticle_ImgeBytes() != null) {
                    secondCardSentImg.setImageBitmap(Constants.decodeBase64(list.get(1).getArticle_ImgeBytes()));
                }
            }
//        }

        }else{
            sentLayout.setEnabled(false);
        }
    }
    /**
     * sharedHasSavedList to check if the shared has savedList and populate the 2 static cards with the data found
     * and handles visibility
     */
    private void sharedHasSavedList(){
        if (sharedPref.contains("savedList")) {
            String savedList = sharedPref.getString("savedList", null);
            ArrayList<TopicsModel>  list = new ArrayList<>(Arrays.asList(gson.fromJson(savedList, TopicsModel[].class)));
            if (list.size() == 1) {
                savedLayout.setEnabled(true);
                nothingSavedToShow.setVisibility(View.INVISIBLE);
                firstSavedCard.setVisibility(View.VISIBLE);
                firstCardSavedTitle.setText(list.get(0).getArticle_Title());
                if (list.get(0).getArticle_ImgeBytes() != null) {
                    firstCardSavedImg.setImageBitmap(Constants.decodeBase64(list.get(0).getArticle_ImgeBytes()));
                }

            } else if (list.size() == 0) {
                savedLayout.setEnabled(false);
                Log.e("TAG", "saved list is empty");
            } else {
                savedLayout.setEnabled(true);
                nothingSavedToShow.setVisibility(View.INVISIBLE);
                secondSavedCard.setVisibility(View.VISIBLE);
                firstSavedCard.setVisibility(View.VISIBLE);
                firstCardSavedTitle.setText(list.get(0).getArticle_Title());
                if (list.get(0).getArticle_ImgeBytes() != null) {
                    firstCardSavedImg.setImageBitmap(Constants.decodeBase64(list.get(0).getArticle_ImgeBytes()));
                }
                secondCardSavedTitle.setText(list.get(1).getArticle_Title());
                if (list.get(1).getArticle_ImgeBytes() != null) {
                    secondCardSavedImg.setImageBitmap(Constants.decodeBase64(list.get(1).getArticle_ImgeBytes()));
                }
            }
        }else{
            savedLayout.setEnabled(false);
        }
    }
    private void findViews(){
        firstCardSentImg = (ImageView)findViewById(R.id.first_card_sent_news_image);
        firstCardSavedImg = (ImageView) findViewById(R.id.first_card_saved_news_image);
        secondCardSavedImg =(ImageView) findViewById(R.id.second_card_saved_news_image);
        secondCardSentImg= (ImageView)findViewById(R.id.second_card_sent_news_image);
        firstSavedCard= (CardView) findViewById(R.id.first_card_saved_news);

        nothingSavedToShow = (TextView) findViewById(R.id.no_saved_news_to_show);
        secondCardSentTitle= (TextView) findViewById(R.id.second_card_sent_news_title);
        secondCardSavedTitle= (TextView) findViewById(R.id.second_card_saved_news_title);
        firstCardSavedTitle= (TextView) findViewById(R.id.first_card_saved_news_title);
        firstCardSentTitle = (TextView) findViewById(R.id.first_card_news_sent_title);

        secondSavedCard= (CardView) findViewById(R.id.second_card_saved_news);
        secondCardSentImg=(ImageView) findViewById(R.id.second_card_sent_news_image);
        sentLayout = (LinearLayout)findViewById(R.id.sent_news_text_layout);
        savedLayout = (LinearLayout)findViewById(R.id.saved_news_text_layout);
        nothingSentToShow = (TextView) findViewById(R.id.no_sent_news_to_show);
        c2= (CardView) findViewById(R.id.sent_news_second_card);
        c1= (CardView) findViewById(R.id.sent_news_first_card);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        assert toolbar!=null;
        setSupportActionBar(toolbar);
        FloatingActionButton addNewsBtn = (FloatingActionButton) findViewById(R.id.fab);
        findViews();
        gson = new Gson();
        sharedPref = getSharedPreferences("myPrefs", Context.MODE_APPEND);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            toolbar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }



        Constants.changeStatusBarColor(getWindow(), MainActivity.this);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //get the saved list from shared and populate the cards
        sharedHasSavedList();
        //get the sent list from shared and populate the cards
        sharedHasSentList();
        assert addNewsBtn != null;

        firstSavedCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddNewsActivity.class);
                intent.putExtra("openFirstSavedCard", "");
                startActivity(intent);
            }

        });

        secondSavedCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddNewsActivity.class);
                intent.putExtra("openSecondSavedCard", "");
                startActivity(intent);
            }

        });

        savedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AllSavedNews.class);
                startActivity(intent);

            }
        });

        sentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AllSentNews.class);
                startActivity(intent);

            }
        });
        addNewsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddNewsActivity.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
                sharedPref = getSharedPreferences("myPrefs", Context.MODE_APPEND);
                SharedPreferences.Editor editor ;
                editor = sharedPref.edit();
                editor.putInt("firstLogin", 0);
                editor.apply();
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Intent thisIntent = MainActivity.this.getIntent();
        startActivity(thisIntent);
        startActivity(intent);
        finish();

    }

}