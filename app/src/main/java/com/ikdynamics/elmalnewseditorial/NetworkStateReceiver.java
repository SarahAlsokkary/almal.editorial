package com.ikdynamics.elmalnewseditorial;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkStateReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = "CheckNetworkStatus";
    private static boolean isConnected = false;

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(final Context context, final Intent intent) {

        Log.e(LOG_TAG, "Received notification about network status");
        isNetworkAvailable(context);

    }

    /**
     *isNetworkAvailable to find if the device is connected to internet or not
     * @param context receiver context
     * @return true if the device is connected to internet
     *         false if the device is not connected
     */
    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null) {
                    if (info.getState() == NetworkInfo.State.CONNECTED) {
                        if(!isConnected){
                            Log.e(LOG_TAG, "Now you are connected to Internet!");
                            isConnected = true;
                            context.startService(new Intent(context, MyService.class));
                        }
                        return true;
                    }
            }
        }
        Log.e(LOG_TAG, "You are not connected to Internet!");
        isConnected = false;
        return false;
    }
}